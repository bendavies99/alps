<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Auth')->group(function () {
    Route::prefix('auth')->group(function () {
        Route::post('register', 'AuthController@register');
        Route::post('login', 'AuthController@login');
        Route::middleware('jwt.auth')->group(function () {
            Route::post('logout', 'AuthController@logout');
            Route::get('/user', function (Request $request) {
                return $request->user();
            });
            Route::put('/update', 'AuthController@update');
            Route::put('/updatePassword', 'AuthController@updatePassword');
            Route::post('/deleteUser', 'AuthController@delete');
            Route::get('/download', 'AuthController@download');
        });
    });
});

Route::prefix('module')->group(function () {
    Route::middleware('jwt.auth')->group(function () {
        Route::get('/', 'ModuleController@index');
        Route::get('/{module}', 'ModuleController@show');
    });
});

Route::prefix('lesson')->group(function () {
    Route::middleware('jwt.auth')->group(function () {
        Route::get('/{lesson}', 'LessonController@show');
        Route::put('/start/{lesson}', 'LessonController@start');
        Route::put('/finish/{lesson}', 'LessonController@finish');
    });
});

Route::prefix('board')->group(function () {
    Route::middleware('jwt.auth')->group(function () {
        Route::get('/', 'BoardController@index');
        Route::get('/{board}', 'BoardController@show');
        Route::post('/', 'BoardController@store');
        Route::put('/{board}', 'BoardController@update');
        Route::put('/{board}/add-member', 'BoardController@addMember');
        Route::put('/{board}/remove-member', 'BoardController@removeMember');
        Route::delete('/{board}', 'BoardController@destroy');
    });
});

Route::prefix('issue')->group(function () {
    Route::middleware('jwt.auth')->group(function () {
        Route::post('/', 'IssueController@store');
        Route::put('/{issue}', 'IssueController@update');
        Route::put('/{issue}/add-sprint', 'IssueController@addToSprint');
        Route::put('/{issue}/remove-sprint', 'IssueController@removeFromSprint');
        Route::put('/{issue}/add-link', 'IssueController@storeLink');
        Route::put('/{issue}/update-link', 'IssueController@updateLink');
        Route::put('/{issue}/remove-link', 'IssueController@destroyLink');
        Route::put('/{issue}/add-sub', 'IssueController@storeSub');
        Route::delete('/{issue}', 'IssueController@destroy');
    });
});

Route::prefix('sprint')->group(function () {
    Route::middleware('jwt.auth')->group(function () {
        Route::post('/', 'SprintController@store');
        Route::put('/{sprint}', 'SprintController@update');
        Route::delete('/{sprint}', 'SprintController@destroy');
    });
});

Route::prefix('user')->group(function () {
    Route::middleware('jwt.auth')->group(function () {
        Route::get('/search/{search}', 'UserController@findBySearch');
    });
});
