import { getStoreBuilder } from "vuex-typex";
import state, { RootState } from "../store";

export interface IUser {
    id: number;
    name: string;
    email: string;
}

export interface UserState {
    user: IUser;
}

export const initalState: UserState = {
    user: {
        id: -1,
        name: "",
        email: ""
    }
};

const builder = getStoreBuilder<RootState>().module("user", initalState);
/**
 * GETTERS
 */
const getId = builder.read((state: UserState) => state.user.id, "userId");
const getName = builder.read(
    (state: UserState) => state.user.name,
    "userFullName"
);
const getEmail = builder.read(
    (state: UserState) => state.user.email,
    "userEmail"
);

/**
 * MUTATIONS
 */
const setUser = (state: UserState, user: IUser) => (state.user = user);

export const user = {
    get userId() {
        return getId();
    },
    get name() {
        return getName();
    },
    get email() {
        return getEmail();
    },
    setUser: builder.commit(setUser, "setUser")
};
