import { getStoreBuilder, BareActionContext } from "vuex-typex";
import { RootState } from "../store";
import { IAPIResponse } from "../api/APIUtil";

export interface SnackbarState {
    snackBarVisibility: boolean;
    snackBarMessage: string;
    snackBarColor: string;
    snackBarUndo: boolean;
}

interface ISnackbarPayload {
    message: string;
    color: string;
    showUndo: boolean;
}

const initialState: SnackbarState = {
    snackBarVisibility: false,
    snackBarMessage: "",
    snackBarColor: "",
    snackBarUndo: false
};

const builder = getStoreBuilder<RootState>().module("snackbar", initialState);

/**
 * GETTERS
 */
const getIsSnackbarVisible = builder.read(
    (state: SnackbarState) => state.snackBarVisibility,
    "snackbarIsVisible"
);
const getSnackbarMessage = builder.read(
    (state: SnackbarState) => state.snackBarMessage,
    "snackbarMessage"
);
const getSnackbarColor = builder.read(
    (state: SnackbarState) => state.snackBarColor,
    "snackbarColor"
);

const getSnackbarShowUndo = builder.read(
    (state: SnackbarState) => state.snackBarUndo,
    "snackbarShowUndo"
);

/**
 * MUTATIONS
 */

const showSnackbar = (state: SnackbarState, payload: ISnackbarPayload) => {
    state.snackBarVisibility = !state.snackBarVisibility;
    state.snackBarMessage = payload.message;
    state.snackBarColor = payload.color;
    state.snackBarUndo = payload.showUndo;
};

const showSuccessSnackbar = (state: SnackbarState, message: string) =>
    showSnackbar(state, { message, color: "success", showUndo: false });
const showFailedSnackbar = (state: SnackbarState, message: string) =>
    showSnackbar(state, { message, color: "red", showUndo: false });

export const snackbar = {
    get isSnackbarVisible() {
        return getIsSnackbarVisible();
    },
    get snackbarMessage() {
        return getSnackbarMessage();
    },
    get snackbarColor() {
        return getSnackbarColor();
    },
    get showUndo() {
        return getSnackbarShowUndo();
    },
    showSnackbar: builder.commit(showSnackbar, "showSnackbar"),
    showSuccessSnackbar: builder.commit(
        showSuccessSnackbar,
        "showSuccessSnackbar"
    ),
    showFailedSnackbar: builder.commit(showFailedSnackbar, "showFailedSnackbar")
};
