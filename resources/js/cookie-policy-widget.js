window.privacyChoicesConfiguration = {
    policy: {
        display: true,
        uri: '/privacy'
    },
    language: {
        prompt: {
            heading: 'Privacy information',
            description: 'This site uses cookies to enable core functionality. Such as allowing you to authenticate with ALPS',
            settingsButton: 'More information',
            dismissButton: 'Dismiss'
        },
        settings: {
            openButton: 'Privacy information',
            closeButton: 'Close',
            heading: 'Privacy information',
            description: 'This site uses cookies to enable core functionality. Such as allowing you to authenticate with ALPS'
        },
        policy: {
            text: 'For more information on our use of web storage, please refer to ALPS\'s ',
            linkText: 'privacy policy'
        }
    },
    style: {
        toggleBackgroundColour: '#222222',
        toggleTextColour: '#ffffff',
        buttonBackgroundColour: '#07748C',
        buttonTextColour: '#ffffff',
        promptBackgroundColour: '#222222',
        promptTextColour: '#ffffff',
        settingsBackgroundColour: '#222222',
        settingsTextColour: '#ffffff'
    }
}
