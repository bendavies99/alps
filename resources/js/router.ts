import { snackbar } from "./store/Snackbar";
import { getUser } from "./api/Auth";
import { APIUtil } from "./api/APIUtil";
import Vue from "vue";
import VueRouter, { Route } from "vue-router";
import Home from "./views/Home.vue";
import About from "./views/About.vue";
import LearningDashboard from "./views/LearningDashboard.vue";
import Module from "./views/Module.vue";
import Lesson from "./views/Lesson.vue";
import Boards from "./views/Boards.vue";
import Board from "./views/Board.vue";
import BoardBacklog from "./views/BoardBacklog.vue";
import BoardSprint from "./views/BoardSprint.vue";
import BoardSettings from "./views/BoardSettings.vue";
import BoardIssue from "./views/BoardIssue.vue";
import Terms from "./views/Terms.vue";
import Privacy from "./views/Privacy.vue";
import UserSettings from "./views/UserSettings.vue";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "index",
            component: Home
        },
        {
            path: "/about",
            name: "about",
            component: About
        },
        {
            path: "/terms",
            name: "Terms",
            component: Terms
        },
        {
            path: "/privacy",
            name: "Privacy",
            component: Privacy
        },
        {
            path: "/learning",
            name: "learning-dashboard",
            component: LearningDashboard,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/user/settings",
            name: "user-settings",
            component: UserSettings,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/boards",
            name: "boards-home",
            component: Boards,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/module/:id",
            name: "module",
            component: Module,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/board/:id",
            component: Board,
            meta: {
                requiresAuth: true
            },
            children: [
                {
                    path: "",
                    component: BoardBacklog,
                    props: true,
                    meta: {
                        requiresAuth: true
                    }
                },
                {
                    path: "settings",
                    component: BoardSettings,
                    props: true,
                    meta: {
                        requiresAuth: true
                    }
                },
                {
                    path: "sprint",
                    component: BoardSprint,
                    props: true,
                    meta: {
                        requiresAuth: true
                    }
                },
                {
                    path: "issue/:isId",
                    component: BoardIssue,
                    props: true,
                    meta: {
                        requiresAuth: true
                    }
                }
            ]
        },
        {
            path: "/lesson/:id",
            name: "lesson",
            component: Lesson,
            meta: {
                requiresAuth: true
            }
        }
    ]
});

router.beforeResolve((to: Route, from: Route, next: CallableFunction) => {
    getUser()
        .then(u => next())
        .catch(e => {
            console.log(to);
            if (to.meta.requiresAuth) {
                next(from.fullPath);
                snackbar.showFailedSnackbar(
                    "You must be logged into ALPS to access Learning or Boards"
                );
            } else {
                next();
            }
        });
});

export default router;
