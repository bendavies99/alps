import { IUser } from "./Auth";
import { APIUtil } from "./APIUtil";
import { IIssue } from "./Issue";
import { ISprint } from "./Sprint";
export interface IBoard {
    name: string;
    id: string;
    type: "scrum" | "kanban";
    openIssues: number;
    sprints: ISprint[];
    issues: IIssue[];
    users: IUser[];
}

// CRUD
export const createBoard = async (board: IBoard) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "board",
        "post",
        board
    );
};

export const getAllBoards = async (): Promise<IBoard[]> => {
    return (await APIUtil.getData("board")).data;
};

export const getBoard = async (id: string): Promise<IBoard> => {
    return (await APIUtil.getData("board/" + id)).data;
};

export const addMemberToBoard = async (
    id: string,
    data: { userId: number; permission: string }
) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "board/" + id + "/add-member",
        "put",
        data
    );
};

export const removeMemberFromBoard = async (
    id: string,
    data: { userId: number }
) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "board/" + id + "/remove-member",
        "put",
        data
    );
};

export const updateBoard = async (id: string, board: IBoard) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "board/" + id,
        "put",
        board
    );
};

export const deleteBoard = async (id: string) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "board/" + id,
        "delete",
        undefined
    );
};
