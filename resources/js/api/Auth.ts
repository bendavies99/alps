import { APIUtil } from "./APIUtil";
export interface IUser {
    name: string;
    email: string;
}

export const getUser = async () => {
    return await APIUtil.getData("auth/user", false);
};

export const deleteUser = async (password: string) => {
    return await APIUtil.runCommand(
        { createSnackbar: true },
        "auth/deleteUser",
        "post",
        {
            password
        }
    );
};

export const downloadData = async () => {
    const response = await APIUtil.getAxiosInst().get("/auth/download", {
        responseType: "blob"
    });
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute("download", "data.json");
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
};

export const updateUser = async (name: string, email: string) => {
    return await APIUtil.runCommand(
        { createSnackbar: true },
        "auth/update",
        "put",
        { name, email }
    );
};

export const updateUserPassword = async (
    oldPass: string,
    newPass: string,
    confPass: string
) => {
    try {
        return await APIUtil.runCommand(
            { createSnackbar: true },
            "auth/updatePassword",
            "put",
            { oldPass, newPass, confPass }
        );
    } catch (e) {
        throw e;
    }
};

export const login = async (email: string, password: string) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: false },
        "auth/login",
        "post",
        {
            email,
            password
        }
    );
};

export const register = async (
    name: string,
    email: string,
    password: string
) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: false },
        "/auth/register",
        "post",
        { name, email, password }
    );
};

export const logout = async () => {
    return await APIUtil.runCommand(
        { createSnackbar: true },
        "auth/logout",
        "post",
        {}
    );
};

export const forgotPassword = async (email: string) => {
    return await APIUtil.runCommand(
        { createSnackbar: true },
        "auth/forgot",
        "post",
        {
            email
        }
    );
};
