import { APIUtil } from "./APIUtil";
import { LessonType, ILesson, Progress } from "./Lesson";

export interface IModule {
    id: number;
    name: string;
    description: string;
    includes: LessonType[];
    lessons: ILesson[];
}

// CRUD

export const getAllModules = async (): Promise<IModule[]> => {
    return (await APIUtil.getData("/module", false)).data;
};

export const getModule = async (id: string): Promise<IModule> => {
    return (await APIUtil.getData("/module/" + id, false)).data;
};
