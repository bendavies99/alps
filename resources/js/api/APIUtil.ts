import { snackbar } from "./../store/Snackbar";
import Axios, { AxiosInstance, AxiosError, AxiosResponse } from "axios";
import EventBus from "../EventBus";

class APIUtilImpl {
    private axios: AxiosInstance;

    constructor() {
        this.axios = Axios.create({
            baseURL:
                process.env.NODE_ENV === "development"
                    ? "http://localhost:8000/api"
                    : "https://alps.bdavies.net/api",
            withCredentials: true
        });

        this.axios.defaults.headers["x-front-end"] = true;
    }

    public getAxiosInst() {
        return this.axios;
    }

    private async getResponse(
        path: string,
        type: "post" | "put" | "delete",
        data: any
    ) {
        switch (type) {
            case "post":
                return await this.axios.post(path, data);
            case "put":
                return await this.axios.put(path, data);
            case "delete":
                return await this.axios.delete(path);
        }
    }

    async getData(path: string, showSnackbar?: boolean) {
        try {
            return (await this.axios.get(path)).data;
        } catch (err) {
            if (err.response && showSnackbar) {
                const res = err.response;
                if (res.status === 401) {
                    snackbar.showFailedSnackbar("Operation Not Perimitted!");
                    throw err;
                } else if (res.status === 500) {
                    snackbar.showFailedSnackbar(
                        "Error: Server error, If this persits, please contact IT Support."
                    );
                    throw err;
                } else if (res.status === 400) {
                    snackbar.showFailedSnackbar(
                        "Error: Server could not accept data, it was presented with. Please try again making sure everyting is correct."
                    );
                    throw err.response.data.message[0];
                } else if (res.status === 403) {
                    snackbar.showFailedSnackbar(
                        "Error: You are not authorized to do this action."
                    );
                    throw err;
                } else {
                    snackbar.showFailedSnackbar(res.data.failed.message);
                    throw err;
                }
            } else {
                if (err.response) {
                    throw err.response;
                } else {
                    throw err;
                }
            }
        }
    }

    async runCommand(
        opts: IAPIOptions,
        path: string,
        type: "post" | "put" | "delete",
        data: any
    ) {
        if (opts.createSnackbar == undefined) opts.createSnackbar = true;
        return opts.createSnackbar
            ? this.createSnackbar(
                  this.getResponse(path, type, data) as Promise<
                      AxiosResponse<IAPIResponse>
                  >,
                  opts.onSuccess ? opts.onSuccess : () => {},
                  opts.onFailed ? opts.onFailed : () => {},
                  opts.showFailSnackbar === undefined
                      ? true
                      : opts.showFailSnackbar
              )
            : ((await this.getResponse(path, type, data)) as AxiosResponse<
                  IAPIResponse
              >).data;
    }

    createSnackbar = (
        promise: Promise<AxiosResponse<IAPIResponse>>,
        onSuccess: CallableFunction,
        onFailed: CallableFunction,
        showFailed: boolean
    ): Promise<any> => {
        let newPromise = new Promise((resolve, reject) => {
            promise
                .then((resultAR: AxiosResponse<IAPIResponse>) => {
                    const result: IAPIResponse = resultAR.data;
                    if (onSuccess) onSuccess(result);

                    snackbar.showSnackbar({
                        message: result.success.message || "",
                        color: "success",
                        showUndo: true
                    });
                    const handler = () => {
                        result.wantsUndo = true;
                    };
                    EventBus.$on("snackbar-undo-clicked", handler);
                    const offHandler = () => {
                        EventBus.$off("snackbar-undo-clicked", handler);
                        EventBus.$off("snackbar-close", offHandler);
                    };
                    EventBus.$on("snackbar-close", offHandler);
                    return resolve(result);
                })
                .catch((err: AxiosError) => {
                    if (err.response && showFailed) {
                        const res = err.response;
                        if (res.status === 401) {
                            snackbar.showFailedSnackbar(
                                "Operation Not Perimitted!"
                            );
                        } else if (res.status === 500) {
                            snackbar.showFailedSnackbar(
                                "Error: Server error, If this persits, please contact IT Support."
                            );
                        } else if (res.status === 400) {
                            snackbar.showFailedSnackbar(
                                "Error: Server could not accept data, it was presented with. Please try again making sure everyting is correct."
                            );
                            reject(err.response.data.message[0]);
                        } else if (res.status === 403) {
                            snackbar.showFailedSnackbar(
                                "Error: You are not authorized to do this action."
                            );
                        } else {
                            if (onFailed) {
                                onFailed();
                            } else {
                                snackbar.showFailedSnackbar(
                                    res.data.failed.message
                                );
                                reject(err);
                            }
                        }
                    } else {
                        if (err.message && showFailed) {
                            snackbar.showFailedSnackbar(err.message);
                        }
                        reject(err);
                    }
                });
        });
        return newPromise;
    };
}

export interface IAPIResponse {
    status?: number;
    success: {
        message?: string;
        item?: any;
        items?: any;
    };
    wantsUndo?: boolean;
}

export interface IAPIOptions {
    createSnackbar?: boolean;
    showFailSnackbar?: boolean;
    onSuccess?: CallableFunction;
    onFailed?: CallableFunction;
}

export const APIUtil = new APIUtilImpl();
