import { APIUtil } from "./APIUtil";
export enum Progress {
    NOT_STARTED,
    STARTED,
    FINISHED
}

export enum LessonType {
    THEORY,
    PRACTICAL,
    QUIZ
}

export interface Question {
    qNo: number;
    title: string;
    answers: any[];
    answer: number;
}

export interface ILesson {
    id: number;
    name: string;
    description: string;
    estToComplete: string;
    progress: Progress;
    includes: LessonType[];
    theory: string;
    practical: any;
    quiz: Question[];
    moduleId: number;
}

export const getLesson = async (id: string): Promise<ILesson> => {
    return (await APIUtil.getData("lesson/" + id)).data;
};

export const startLesson = async (id: string) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: false },
        "lesson/start/" + id,
        "put",
        undefined
    );
};

export const finishLesson = async (id: string) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: false },
        "lesson/finish/" + id,
        "put",
        undefined
    );
};
