import { APIUtil } from "./APIUtil";
import { ISprint } from "./Sprint";

export interface IIssueLink {
    type: "blocks" | "blocking";
    issueId: string;
}

export interface IIssue {
    name: string;
    id: string;
    type: "task" | "bugfix";
    progress: "ToDo" | "In Progress" | "Done";
    sprint?: ISprint;
    description: string;
    children: IIssue[];
    links: IIssueLink[];
    assignedToId?: number;
}

export interface ICreateIssue {
    name: string;
    id: string;
    type: "task" | "bugfix";
    progress: "ToDo" | "In Progress" | "Done";
    description: string;
    board_id: string;
}

export interface IUpdateIssue {
    name: string;
    id: string;
    type: "task" | "bugfix";
    progress: "ToDo" | "In Progress" | "Done";
    description: string;
    assignId?: number;
}

export const createIssue = async (issue: ICreateIssue) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "issue",
        "post",
        issue
    );
};

export const createSubIssue = async (id: string, issue: IIssue) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "issue/" + id + "/add-sub",
        "put",
        issue
    );
};

export const updateIssue = async (id: string, issue: IUpdateIssue) => {
    return await APIUtil.runCommand(
        { createSnackbar: false, showFailSnackbar: true },
        "issue/" + id,
        "put",
        issue
    );
};

export const deleteIssue = async (id: string) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "issue/" + id,
        "delete",
        undefined
    );
};

export const createIssueLink = async (
    issueId: string,
    issueLink: IIssueLink
) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "issue/" + issueId + "/add-link",
        "put",
        issueLink
    );
};
export const updateIssueLink = async (
    issueId: string,
    issueLink: IIssueLink
) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "issue/" + issueId + "/update-link",
        "put",
        issueLink
    );
};
export const deleteIssueLink = async (
    issueId: string,
    issueLink: IIssueLink
) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "issue/" + issueId + "/remove-link",
        "put",
        issueLink
    );
};

export const addToSprint = async (issueId: string, sprint_id: string) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "issue/" + issueId + "/add-sprint",
        "put",
        { sprint_id }
    );
};

export const removeFromSprint = async (issueId: string) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "issue/" + issueId + "/add-sprint",
        "put",
        undefined
    );
};
