import { APIUtil } from "./APIUtil";
export interface IUser {
    name: string;
    email: string;
}

export const getUserBySearch = async (searchTerm: string) => {
    return await APIUtil.getData("/user/search/" + searchTerm, false);
};
