import { APIUtil } from "./APIUtil";
import { IIssue } from "./Issue";
export interface ISprint {
    name: string;
    id: string;
    startDate: string;
    endDate: string;
    issues: IIssue[];
}

export interface ICreateSprint {
    name: string;
    id: string;
    startDate: string;
    endDate: string;
    issues: string[];
    board_id: string;
}

export const createASprint = async (sprint: ICreateSprint) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "sprint",
        "post",
        sprint
    );
};

export const updateASprint = async (id: string, sprint: ISprint) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "sprint/" + id,
        "put",
        sprint
    );
};

export const deleteASprint = async (id: string) => {
    return await APIUtil.runCommand(
        { createSnackbar: true, showFailSnackbar: true },
        "sprint/" + id,
        "delete",
        undefined
    );
};
