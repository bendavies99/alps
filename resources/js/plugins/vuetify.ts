import Vue from "vue";
import Vuetify, { VuetifyPreset } from "vuetify";

Vue.use(Vuetify);
const opts: Partial<VuetifyPreset> = {
    icons: {
        iconfont: "md",
        values: {}
    },
    theme: {
        dark: false,
        default: "light",
        disable: false,
        options: {},
        themes: {
            light: {
                primary: "#07748C",
                secondary: "#424242",
                accent: "#82B1FF",
                error: "#D94F4F",
                info: "#2196F3",
                success: "#4CAF50",
                warning: "#FB8C00"
            },
            dark: {
                primary: "#1976D2",
                secondary: "#424242",
                accent: "#82B1FF",
                error: "#D94F4F",
                info: "#2196F3",
                success: "#4CAF50",
                warning: "#FB8C00"
            }
        }
    }
};

export default new Vuetify(opts);
