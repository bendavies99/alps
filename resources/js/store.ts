import Vue from "vue";
import Vuex from "vuex";
import { getStoreBuilder } from "vuex-typex";
import { SnackbarState } from "./store/Snackbar";
import { UserState } from "./store/User";

Vue.use(Vuex);

export interface RootState {
    snackbar: SnackbarState;
    user: UserState;
}

const state = getStoreBuilder<RootState>().vuexStore();
export default state;
