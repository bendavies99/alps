<?php

namespace App;


class ScrumBoard extends Board
{
    protected $table = "scrum_boards";
    protected $isSubclass = true;

    public function sprints()
    {
        return $this->hasMany('App\Sprint', 'board_id', 'id');
    }

    public function issues()
    {
        return $this->hasMany(ScrumIssue::class, 'board_id', 'id');
    }

    public function getType()
    {
        return "scrum";
    }
}
