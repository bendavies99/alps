<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sprint extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';

    public function issues()
    {
        return $this->hasMany('App\ScrumIssue');
    }
}
