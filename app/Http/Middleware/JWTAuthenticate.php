<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\UnauthorizedException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Token;

class JWTAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (!$request->headers->has('x-front-end')) throw new UnauthorizedException("Please provide X-Front-End Token");

            $rawToken = $request->cookie('token');
            $token = new Token($rawToken);
            JWTAuth::setToken($token)->authenticate();
        } catch (\Exception $e) {
            return response('Unauthorized.', 401);
        }
        return $next($request)->withCookie(
            'token',
            JWTAuth::refresh(),
            60 * 60,
            '/',
            config('app.domain'),
            false,
            true
        );
    }
}
