<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Validation\UnauthorizedException;

class CheckForFrontEndHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (!$request->headers->has('x-front-end')) {
                throw new UnauthorizedException("Please provide X-Front-End Token");
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Unauthorized.']);
        }
        return $next($request);
    }
}
