<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\IssueResource;

class BoardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "type" => $this->getType(),
            "openIssues" => $this->issues->where('progress', 'ToDo')->count(),
            "sprints" => SprintResource::collection(isset($this->sprints) ? $this->sprints : []),
            "issues" => IssueResource::collection(isset($this->issues) ? $this->issues : []),
            "users" => $this->users,
        ];
    }
}
