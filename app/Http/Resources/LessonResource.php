<?php

namespace App\Http\Resources;

use App\Http\Resources\ProgressResource;

use Illuminate\Http\Resources\Json\JsonResource;

class LessonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $pro = $this->users->where('id', auth()->user()->id)->first();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'progress' => (new ProgressResource(isset($pro) ? $pro->pivot : null))->toArray($request),
            'estToComplete' => $this->estToComplete,
            'theory' => $this->theory,
            'quiz' => QuestionResource::collection(isset($this->questions) ? $this->questions : []),
            'includes' => IncludeResource::collection($this->includes),
            'moduleId' => $this->module_id
        ];
    }
}
