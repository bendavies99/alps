<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SprintResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'id' => $this->id,
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
            'issues' => IssueResource::collection(isset($this->issues) ? $this->issues : []),
        ];
    }
}
