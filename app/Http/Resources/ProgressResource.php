<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProgressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->resource === NULL) {
            return  0;
        }
        if ($this->progress == "NOT_STARTED") {
            return 0;
        } else if ($this->progress == "STARTED") {
            return 1;
        } else {
            return 2;
        }
    }
}
