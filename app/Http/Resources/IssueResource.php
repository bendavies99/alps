<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IssueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $links = [];
        if (isset($this->links)) {
            $links = $this->links->map(function ($link) use ($request) {
                return (new IssueLinkResource($link->pivot))->toArray($request);
            });
        }
        return [
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->description,
            "type" => $this->type,
            "progress" => $this->progress,
            "children" => IssueResource::collection(isset($this->children) ? $this->children : []),
            "links" => $links,
            "sprint" => isset($this->sprint) ? $this->sprint : null,
            "assignedToId" => $this->board_user_id
        ];
    }
}
