<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IncludeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->resource === NULL) {
            return  0;
        }
        if ($this->type === "THEORY") {
            return 0;
        } else if ($this->type === "PRACTICAL") {
            return 1;
        } else {
            return 2;
        }
    }
}
