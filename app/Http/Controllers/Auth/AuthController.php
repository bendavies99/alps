<?php

namespace App\Http\Controllers\Auth;

use App\Board;
use App\Http\Controllers\BoardController;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Resources\BoardResource;
use App\Http\Resources\ModuleResource;
use App\Module;
use App\ScrumBoard;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use JWTAuth;

class AuthController extends Controller
{
    //

    public function register(Request $request)
    {
        $user = User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'name' => $request->name
        ]);

        return response()->json(['success' => ['message' => 'Successfully registered: ' . $request->name]]);
    }

    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken("Successfully Logged in", $token);
    }

    public function delete(Request $request)
    {
        $user = User::where('id', auth()->user()->id)->first();
        if (Hash::check($request->password, $user->password)) {
            DB::transaction(function () use ($user) {
                $boardsOwn = DB::table('board_user')->where([['user_id', $user->id], ['permission', 'Owner']])->get();
                $boardsOwn->each(function ($item, $key) {
                    app('App\Http\Controllers\BoardController')->destroy($item->board_id);
                });
                DB::table('board_user')->where('user_id', $user->id)->delete();
                DB::table('lesson_user')->where('user_id', $user->id)->delete();
                $user->delete();
            });
            return response()->json(['success' => ['message' => 'Successfully deleted user.']]);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }

    public function download()
    {
        //TODO: Boards, Lessons

        //Note: use Board Controller and Lesson Controller and then paste into JSON file.
        $user = User::where('id', auth()->user()->id)->first();
        $data = array();
        $data["user"] = ["name" => $user->name, "email" => $user->email];
        $data["boards"] = BoardResource::collection(ScrumBoard::getAllYourBoards($user));
        $data["modules"] = ModuleResource::collection(Module::all());

        return response()->json($data);
    }

    public function update(Request $request)
    {
        if ($request->name !== "" && isset($request->name) && $request->email !== "" && isset($request->email)) {
            $user = User::where('id', auth()->user()->id)->first();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();
            return response()->json(['success' => ['message' => 'Successfully updated user.']]);
        } else {
            return response()->json(['error' => 'Cannot use empty values'], 403);
        }
    }

    public function updatePassword(Request $request)
    {
        if ($request->oldPass !== "" && isset($request->oldPass) && $request->newPass !== "" && isset($request->newPass) && $request->confPass !== "" && isset($request->confPass)) {
            $user = User::where('id', auth()->user()->id)->first();
            if (Hash::check($request->oldPass, $user->password)) {
                if ($request->newPass === $request->confPass) {
                    $user->password = Hash::make($request->newPass);
                    $user->save();
                    return response()->json(['success' => ['message' => 'Successfully updated password.']]);
                } else {
                    return response()->json(['error' => 'Unauthorized'], 401);
                }
            } else {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
        } else {
            return response()->json(['error' => 'Cannot use empty values'], 403);
        }
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['success' => ['message' => 'Successfully logged out']])->cookie('token', '', -1, '/', config('app.domain'), false, true);
    }

    protected function respondWithToken($message, $token)
    {
        return response()->json([
            'success' => [
                'message' => $message,
                'item' => [
                    'user' => JWTAuth::user(),
                    'access_token' => $token,
                    'token_type'   => 'bearer',
                    'expires_in'   => auth('api')->factory()->getTTL() * 60 * 8
                ]
            ],
        ])->cookie('token', $token, 60 * 6, '/', config('app.domain'), false, true);
    }
}
