<?php

namespace App\Http\Controllers;

use App\Sprint;
use DB;
use Illuminate\Http\Request;

class SprintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $sprint = new Sprint;
            $sprint->id = $request->id;
            $sprint->name = $request->name;
            $sprint->startDate = $request->startDate;
            $sprint->endDate = $request->endDate;
            $sprint->board_id = $request->board_id;
            $sprint->save();

            foreach ($request->issues as $issue) {
                DB::table('scrum_issues')->where('id', $issue)->update([
                    'sprint_id' => $request->id
                ]);
            }
        });

        return response()->json(['success' => ['message' => 'Successfully created sprint']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sprint  $sprint
     * @return \Illuminate\Http\Response
     */
    public function show(Sprint $sprint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sprint  $sprint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sprint $sprint)
    {
        $sprint->id = $request->id;
        $sprint->name = $request->name;
        $sprint->startDate = $request->startDate;
        $sprint->endDate = $request->endDate;
        $sprint->board_id = $request->board_id;
        $sprint->save();

        return response()->json(['success' => ['message' => 'Successfully updated sprint']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sprint  $sprint
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sprint $sprint)
    {
        //
        $sprint->delete();
        return response()->json(['success' => ['message' => 'Successfully deleted sprint']]);
    }
}
