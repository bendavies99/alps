<?php

namespace App\Http\Controllers;

use App\Http\Resources\LessonResource;
use App\Lesson;
use App\Progress;
use DB;
use Illuminate\Http\Request;

class LessonController extends Controller
{



    /**
     * Display the specified resource.
     *
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function show(Lesson $lesson)
    {
        return new LessonResource($lesson);
    }

    public function start(Request $request, Lesson $lesson)
    {
        if ($lesson->users->where('id', auth()->user()->id)->count() !== 0) {
            $p = $lesson->users->where('id', auth()->user()->id)->first()->pivot;
            $p->progress = "STARTED";
            $p->save();
        } else {
            DB::table('lesson_user')->insert(['lesson_id' => $lesson->id, 'user_id' => auth()->user()->id, 'progress' => 'STARTED']);
        }
        return response()->json(['success' => ['message' => 'Successfully started lesson']]);
    }

    public function finish(Request $request, Lesson $lesson)
    {
        if ($lesson->users->where('id', auth()->user()->id)->count() !== 0) {
            $p = $lesson->users->where('id', auth()->user()->id)->first()->pivot;
            $p->progress = "FINISHED";
            $p->save();
        }
        return response()->json(['success' => ['message' => 'Successfully finished lesson']]);
    }
}
