<?php

namespace App\Http\Controllers;

use App\Http\Resources\BoardResource;
use App\ScrumBoard;
use DB;
use Illuminate\Http\Request;

class BoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return BoardResource::collection(ScrumBoard::getAllYourBoards(auth()->user()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->name;
        $id = $request->id;

        DB::table('boards')->insert([
            'id' => $id,
            'name' => $name,
            'description' => '...'
        ]);

        DB::table('scrum_boards')->insert([
            'id' => $id,
        ]);

        DB::table('board_user')->insert([
            'board_id' => $id,
            'user_id' => auth()->user()->id,
            'permission' => "Owner"
        ]);

        return response()->json(['success' => ['message' => 'Successfully created board']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ScrumBoard  $scrumBoard
     * @return \Illuminate\Http\Response
     */
    public function show(string $board)
    {
        return new BoardResource(ScrumBoard::find($board));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ScrumBoard  $scrumBoard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $board)
    {
        $name = $request->name;
        $id = $request->id;
        DB::table('boards')->where('id', $board)->update([
            'id' => $id,
            'name' => $name,
            'description' => '...'
        ]);

        return response()->json(['success' => ['message' => 'Successfully updated board']]);
    }

    public function addMember(Request $request, string $board)
    {
        DB::table('board_user')->insert([
            'board_id' => $board,
            'user_id' => $request->userId,
            'permission' => $request->permission,
        ]);
        return response()->json(['success' => ['message' => 'Successfully updated board']]);
    }

    public function removeMember(Request $request, string $board)
    {
        $user = DB::table('board_user')->where([['board_id', $board], ['user_id', $request->userId]])->first();
        if (isset($user)) {
            if ($user->permission === "Owner") {
                return response()->json(['failed' => ['message' => 'Failed to update board']], 403);
            } else {
                DB::table('board_user')->where([['board_id', $board], ['user_id', $request->userId]])->delete();
            }
        }
        return response()->json(['success' => ['message' => 'Successfully updated board']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ScrumBoard  $scrumBoard
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $board)
    {

        DB::table('boards')->where('id', $board)->delete();

        DB::table('scrum_boards')->where('id', $board)->delete();

        DB::table('board_user')->where('board_id', $board)->delete();

        $issues = DB::table('issues')->where('board_id', $board)->get();

        $issues->each(function ($item, $key) {
            app('App\Http\Controllers\IssueController')->destroy($item->id);
        });
        DB::table('sprints')->where('board_id', $board)->delete();
        return response()->json(['success' => ['message' => 'Successfully deleted board']]);
    }
}
