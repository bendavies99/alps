<?php

namespace App\Http\Controllers;

use App\Issue;
use App\ScrumIssue;
use Illuminate\Http\Request;
use DB;

class IssueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('issues')->insert([
            'id' => $request->id,
            'name' => $request->name,
            'description' => $request->description,
            'type' => $request->type,
            'progress' => 'ToDo',
            'board_id' => $request->board_id
        ]);

        DB::table('scrum_issues')->insert([
            'id' => $request->id
        ]);

        return response()->json(['success' => ['message' => 'Successfully created issue']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ScrumIssue  $scrumIssue
     * @return \Illuminate\Http\Response
     */
    public function show(string $issue)
    {
        //
    }

    public function addToSprint(Request $request, string $issue)
    {
        DB::table('scrum_issues')->where('id', $issue)->update([
            'sprint_id' => $request->sprint_id,
        ]);

        return response()->json(['success' => ['message' => 'Successfully updated issue']]);
    }

    public function removeFromSprint(Request $request, string $issue)
    {
        DB::table('scrum_issues')->where('id', $issue)->update([
            'sprint_id' => null,
        ]);

        return response()->json(['success' => ['message' => 'Successfully updated issue']]);
    }

    public function storeLink(Request $request, string $issue)
    {
        DB::table('issue_link')->insert([
            'issue1_id' => $issue,
            'issue2_id' => $request->issueId,
            'type' => $request->type,
        ]);

        DB::table('issue_link')->insert([
            'issue1_id' => $request->issueId,
            'issue2_id' => $issue,
            'type' => $request->type === "blocks" ? "blocking" : "blocks",
        ]);

        return response()->json(['success' => ['message' => 'Successfully updated issue']]);
    }

    public function destroyLink(Request $request, string $issue)
    {
        DB::table('issue_link')->where(['issue1_id', $issue], ['issue2_id', $request->issueId])->delete();
        DB::table('issue_link')->where(['issue2_id', $issue], ['issue1_id', $request->issueId])->delete();
        return response()->json(['success' => ['message' => 'Successfully updated issue']]);
    }

    public function updateLink(Request $request, string $issue)
    {
        DB::table('issue_link')->where(['issue1_id', $issue], ['issue2_id', $request->issueId])->update([
            'type' => $request->type
        ]);
        DB::table('issue_link')->where(['issue2_id', $issue], ['issue1_id', $request->issueId])->update([
            'type' => $request->type === "blocks" ? "blocking" : "blocks"
        ]);
        return response()->json(['success' => ['message' => 'Successfully updated issue']]);
    }

    public function storeSub(Request $request, string $issue)
    {
        $issueFound = DB::table('issues')->where('id', $issue)->first();

        if (!isset($issueFound)) {
            return response()->json(['failed' => ['message' => 'Issue not found']], 404);
        }

        DB::table('issues')->insert([
            'id' => $request->id,
            'name' => $request->name,
            'description' => $request->description,
            'type' => $request->type,
            'progress' => 'ToDo',
            'board_id' => $issueFound->board_id
        ]);

        DB::table('scrum_issues')->insert([
            'id' => $request->id
        ]);

        DB::table('issue_issue')->insert([
            'parent_issue_id' => $issue,
            'child_issue_id' => $request->id,
        ]);

        return response()->json(['success' => ['message' => 'Successfully updated issue']]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ScrumIssue  $scrumIssue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $issue)
    {

        $issueFound = DB::table('issues')->where('id', $issue)->first();

        if (!isset($issueFound)) {
            return response()->json(['failed' => ['message' => 'Issue not found']], 404);
        }

        DB::table('issues')->where('id', $issue)->update([
            'id' => $request->id,
            'name' => $request->name,
            'description' => $request->description,
            'type' => $request->type,
            'progress' => $request->progress,
            'board_user_id' => isset($request->assignId) ? DB::table('board_user')->where([['user_id', $request->assignId], ['board_id', $issueFound->board_id]])->first()->id : null,
        ]);

        return response()->json(['success' => ['message' => 'Successfully updated issue']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ScrumIssue  $scrumIssue
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $issue)
    {
        $this->deleteIssue($issue);

        return response()->json(['success' => ['message' => 'Successfully deleted issue']]);
    }

    public function deleteIssue(string $issue)
    {
        DB::table('issues')->where('id', $issue)->delete();

        DB::table('scrum_issues')->where('id', $issue)->delete();
        DB::table('issue_link')->where('issue1_id', $issue)->delete();
        DB::table('issue_link')->where('issue2_id', $issue)->delete();

        // Delete all sub-issues.
        // Note: Contains recursion.
        $issues = DB::table('issue_issue')->where('parent_issue_id', $issue)->get();
        foreach ($issues as $issueChild) {
            $this->deleteIssue($issueChild->child_issue_id);
        }
        DB::table('issue_issue')->where('parent_issue_id', $issue)->delete();
    }
}
