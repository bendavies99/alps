<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncludeModel extends Model
{
    //
    protected $table = "includes";
}
