<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Lesson extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('progress');
    }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function includes()
    {
        return $this->belongsToMany('App\IncludeModel', 'include_lesson', 'lesson_id', 'include_id');
    }
}
