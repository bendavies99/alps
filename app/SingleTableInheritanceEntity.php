<?php

namespace App;

use Eloquent;

abstract class SingleTableInheritanceEntity extends Eloquent
{
    // must be overridden and set to true in subclasses
    protected $isSubclass = false;

    //parentTable is important
    protected $parentTable = null;

    public function isSubclass()
    {
        return $this->isSubclass;
    }

    /**
     * Define a one-to-many relationship. 
     * 
     * Override, to match the parent table on the foreign key.
     *
     * @param  string  $related
     * @param  string|null  $foreignKey
     * @param  string|null  $localKey
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hasMany($related, $foreignKey = null, $localKey = null)
    {
        $instance = $this->newRelatedInstance($related);

        $foreignKey = $foreignKey ?: $this->getForeignKey();

        $localKey = $localKey ?: $this->getKeyName();

        $tableName = 'test';
        if ($instance->isSubclass) {
            $tableName = $instance->parentTable;
        } else {
            $tableName = $instance->getTable();
        }

        return $this->newHasMany(
            $instance->newQuery(),
            $this,
            $tableName . '.' . $foreignKey,
            $localKey
        );
    }

    public function newQuery($excludeDeleted = true)
    {
        // If using Laravel 4.0.x then use the following commented version of this command
        // $builder = new Builder($this->newBaseQueryBuilder());
        // newEloquentBuilder() was added in 4.1
        $builder = $this->newEloquentBuilder($this->newBaseQueryBuilder());

        // Once we have the query builders, we will set the model instances so the
        // builder can easily access any information it may need from the model
        // while it is constructing and executing various queries against it.
        $builder->setModel($this)->with($this->with);

        if ($excludeDeleted && $this->softDelete) {
            $builder->whereNull($this->getQualifiedDeletedAtColumn());
        }

        if ($this->parentTable && $this->isSubclass()) {
            $builder->join($this->parentTable, $this->table . '.id', '=', $this->parentTable . '.id');
        }

        return $builder;
    }
}
