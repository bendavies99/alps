<?php

namespace App;

use App\SingleTableInheritanceEntity;

abstract class Board extends SingleTableInheritanceEntity
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $parentTable = 'boards';

    public function users()
    {
        return $this->belongsToMany('App\User', 'board_user', 'board_id', 'user_id', 'id', 'id')->withPivot('permission', 'id');
    }

    public static function getAllYourBoards(User $user)
    {
        return static::all()->reject(function ($board) use ($user) {
            return $board->users->where('id', $user->id)->count() === 0;
        });
    }

    public abstract function getType();
}
