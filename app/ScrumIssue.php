<?php

namespace App;

class ScrumIssue extends Issue
{
    protected $table = "scrum_issues";
    protected $isSubclass = true;

    public function sprint()
    {
        return $this->belongsTo('App\Sprint');
    }
}
