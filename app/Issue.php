<?php

namespace App;

use App\SingleTableInheritanceEntity;

class Issue extends SingleTableInheritanceEntity
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $parentTable = 'issues';

    public function links()
    {
        return $this->belongsToMany(Issue::class, 'issue_link', 'issue1_id', 'issue2_id', 'id', 'id')->withPivot('issue2_id', 'type');
    }

    public function children()
    {
        return $this->belongsToMany('App\Issue', 'issue_issue', 'parent_issue_id', 'child_issue_id', 'id', 'id');
    }
}
