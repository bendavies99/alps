<?php

namespace Tests\Feature;

use App\User;
use Hash;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $user = new User([
            'name' => "Test",
            'email' => 'test@test.com',
            'password' => Hash::make('Password!123')
        ]);

        $user->save();
    }

    /**
     * @test
     */
    public function it_will_register_a_user()
    {
        $response = $this->post('api/auth/register', [
            'name' => "name",
            'email' => 'test2@test.com',
            'password' => 'Password!123'
        ]);

        $response->assertJsonStructure([
            'success' => [
                'message',
                'item' => [
                    'access_token',
                    'token_type',
                    'expires_in',
                ]
            ]
        ]);
    }

    /** 
     * @test 
     */
    public function it_will_log_a_user_in()
    {
        $response = $this->post('api/auth/login', [
            'email'    => 'test@test.com',
            'password' => 'Password!123'
        ]);
        $response->assertJsonStructure([
            'success' => [
                'message',
                'item' => [
                    'access_token',
                    'token_type',
                    'expires_in',
                ]
            ]
        ]);
    }

    /** 
     * @test
     */
    public function it_will_not_log_an_invalid_user_in()
    {
        $response = $this->post('api/auth/login', [
            'email'    => 'test@test.com',
            'password' => 'notALegitPassword'
        ]);

        $response->assertJsonStructure([
            'error',
        ]);
    }
}
