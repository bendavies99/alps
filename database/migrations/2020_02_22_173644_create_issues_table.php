<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issues', function (Blueprint $table) {
            $table->string('id');
            $table->string('name');
            $table->string('description');
            $table->enum('type', ["task", "bugfix"]);
            $table->enum('progress', ["ToDo", "In Progress", "Done"]);
            $table->string('board_id');
            $table->bigInteger('board_user_id')->nullable();
            $table->primary('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issues');
    }
}
