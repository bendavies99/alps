<?php

use Illuminate\Database\Seeder;

class IncludeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('includes')->insert([
            'type' => "THEORY"
        ]);

        DB::table('includes')->insert([
            'type' => "PRACTICAL"
        ]);

        DB::table('includes')->insert([
            'type' => "QUIZ"
        ]);
    }
}
