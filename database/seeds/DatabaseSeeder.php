<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create();
        //$this->call(UsersTableSeeder::class);
        $this->call(IncludeTableSeeder::class);
        $this->call(ModuleTableSeeder::class);
        $this->call(LessonTableSeeder::class);

        // DB::table('progresses')->insert([
        //     'progress' => 'FINISHED',
        //     'user_id' => 1
        // ]);

        // DB::table('lesson_progress')->insert([
        //     'lesson_id' => 1,
        //     'progress_id' => 1
        // ]);

        DB::table('boards')->insert([
            'id' => 'DB',
            'name' => 'Demo Board',
            'description' => 'Test'
        ]);

        DB::table('scrum_boards')->insert([
            'id' => 'DB'
        ]);

        DB::table('board_user')->insert([
            'board_id' => 'DB',
            'user_id' => 1,
            'permission' => "Owner",
        ]);

        DB::table('issues')->insert([
            'id' => 'DB-1',
            'name' => 'Demo Issue',
            'description' => 'A demo issue for fresh version of the site',
            'type' => 'task',
            'progress' => 'ToDo',
            'board_id' => 'DB',
            'board_user_id' => 1
        ]);

        DB::table('scrum_issues')->insert([
            'id' => 'DB-1'
        ]);
    }
}
