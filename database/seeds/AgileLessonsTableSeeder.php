<?php

use Illuminate\Database\Seeder;

class AgileLessonsTableSeeder extends Seeder
{

    const MODULE_ID = 1;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createLesson([
            'name' => 'Introduction to Agile',
            'description' => 'This lesson will introduce you to Agile and what it means to work in agile.',
            'estToComplete' => '10 Minutes',
            'theory' => file_get_contents(
                database_path('seeds/lessonData/IntroductionToAgile.md')
            ),
            'module_id' => self::MODULE_ID,
        ], ["THEORY", "QUIZ"], [
            [
                "title" => "What is agile?",
                "answers" => [
                    [
                        "name" => "A Iterative Approach to creating software",
                        "correctAnswer" => true
                    ],
                    [
                        "name" => "Agile is a piece of software",
                        "correctAnswer" => false
                    ],
                    [
                        "name" => "Agile provides a way to organize staff",
                        "correctAnswer" => false
                    ],
                    [
                        "name" => "A way of downloading software made by Atlassain",
                        "correctAnswer" => false
                    ]
                ]
            ]
        ]);

        // $this->createLesson([
        //     'name' => 'Introduction to Agile2',
        //     'description' => 'This lesson will introduce you to Agile and what it means to work in agile.',
        //     'estToComplete' => '10 Minutes',
        //     'theory' => file_get_contents(
        //         database_path('seeds/lessonData/IntroductionToAgile.md')
        //     ),
        //     'module_id' => self::MODULE_ID,
        // ], ["THEORY"], []);
    }

    private function createQuestion(int $lessonId, string $title, array $answers)
    {
        $id = DB::table('questions')->insertGetId([
            'lesson_id' => $lessonId,
            'title' => $title
        ]);


        foreach ($answers as $answer) {
            DB::table('question_answers')->insert([
                'question_id' => $id,
                'name' => $answer["name"],
                'correct_answer' => isset($answer["correctAnswer"]) ? $answer["correctAnswer"] : false
            ]);
        }
    }

    private function createLesson(array $values, array $includes, array $questions)
    {
        $id = DB::table('lessons')->insertGetId($values);

        foreach ($includes as $include) {
            switch ($include) {
                case "THEORY":
                    DB::table('include_lesson')->insert([
                        'lesson_id' => $id,
                        'include_id' => 1
                    ]);
                    break;
                case "PRACTICAL":
                    DB::table('include_lesson')->insert([
                        'lesson_id' => $id,
                        'include_id' => 2
                    ]);
                    break;
                case "QUIZ":
                    DB::table('include_lesson')->insert([
                        'lesson_id' => $id,
                        'include_id' => 3
                    ]);
                    break;
            }
        }

        foreach ($questions as $question) {
            $this->createQuestion($id, $question["title"], $question["answers"]);
        }
    }
}
