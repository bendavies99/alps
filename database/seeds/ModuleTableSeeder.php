<?php

use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('modules')->insert([
            'name' => 'Agile Basics',
            'description' => 'This module provides you with a basic understanding of Agile and what it is used for and why it is important'
        ]);

        DB::table('modules')->insert([
            'name' => 'Scrum Basics',
            'description' => 'This module provides you with a basic understanding of Scrum and what it is used for and why it is important'
        ]);

        // DB::table('modules')->insert([
        //     'name' => 'Kanban Basics',
        //     'description' => 'This module provides you with a basic understanding of Kanban and what it is used for and why it is important'
        // ]);
    }
}
