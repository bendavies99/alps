<?php

use Illuminate\Database\Seeder;

class LessonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AgileLessonsTableSeeder::class);
        $this->call(ScrumLessonsTableSeeder::class);
        //$this->call(KanbanLessonsTableSeeder::class);
    }
}
