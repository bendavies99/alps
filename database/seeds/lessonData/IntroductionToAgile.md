# Introduction to Agile

## So what is Agile?

Agile works by iteratively splitting up your project into several chunks. This helps teams to release products faster to the consumer and with less problems. Agile will deliver small increments of the software to the consumer. Finally requirements are constantly evaluated and because the nature of Agile these can be addressed quickly. (Atlassian, 2020).

## Why Agile

“Organisations increasingly embrace agile as a technique for managing projects. A full 71 percent of organisations report using agile approaches for their projects sometimes, often, or always” (Pmi.org, 2017) this shows that agile is popular in 2017 for organisations. Mahnic (2010) stated that it is important to prepare students for the increase use of agile in the industry, and teaching these methods is now more important for Computer Science and Software Engineering.

#### References

Mahnic, V. (2010). Teaching Scrum through team-project work: Students' perceptions and teacher's observations.. [pdf] International Journal of Engineering Education, p.2. Available at: https://www.researchgate.net/profile/Viljan_Mahnic/publication/257895006_Teaching_Scrum_through_Team-Project_Work_Students'_Perceptions_and_Teacher's_Observations/links/0c9605260fc39684fc000000/Teaching-Scrum-through-Team-Project-Work-Students-Perceptions-and-Teachers-Observations.pdf [Accessed 11 Feb. 2020].

Pmi.org. (2017). PMI's Pulse of the Profession. [pdf] Available at: https://www.pmi.org/-/media/pmi/documents/public/pdf/learning/thought-leadership/pulse/pulse-of-the-profession-2017.pdf [Accessed 11 Feb. 2020].

Atlassian. (2020). What is Agile? | Atlassian. [online] Available at: https://www.atlassian.com/agile [Accessed 11 Feb. 2020].
