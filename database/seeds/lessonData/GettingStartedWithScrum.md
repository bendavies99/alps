# Getting Started With Scrum

## So what is Scrum?

Scrum is a framework for Agile, it has a few basic concepts you need to understand they are:-

-   **Development Team** - these are the developers creating the product.
-   **The product owner** - is in charge of the product backlog and makes sure that all issues are prioritized, they create sprints but they can cancel sprints if the sprint goal is redundant. Finally they are in charge of talking to the customer. If there is one.
-   **Scrum Master** - They help the team, if any way possible and make sure they keep on track with anything, they also set up retrospectives and meetings with team to keep track of things.
-   **Product Backlog** - This is a list of product issues and these will be on sort of queue before it gets added to a sprint, a user can prioritize these issues in a more advanced system; **ALPS** is designed for beginners so this option is not available.
-   **Issue** - in **ALPS** there are two types of issues to keep it simple and they task and bugfix. Task is a feature or chore task that may have to be done in the product. A bugfix is a more important issue within your system depending on the severity of the bug.
-   **Sprint** - a sprint is a time frame where you and your team promise to complete the issues defined in the sprint. A sprint can have a sprint goal which states what you want to achieve as **ALPS** is staying simple this again not available.

## What's Next?

So in **ALPS** to keep it simple theres a simple boards system in place that helps you design and create your own Scrum board with ease. Please goto the practical section to get started.
