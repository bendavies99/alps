<?php

use Illuminate\Database\Seeder;

class ScrumLessonsTableSeeder extends Seeder
{
    const MODULE_ID = 2;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createLesson([
            'name' => 'Getting Started With Scrum',
            'description' => 'This lesson will introduce you to the scrum framework and then you will make a scrum board and make your first issue.',
            'estToComplete' => '15 Minutes',
            'theory' => file_get_contents(
                database_path('seeds/lessonData/GettingStartedWithScrum.md')
            ),
            'module_id' => self::MODULE_ID,
        ], ["THEORY", "QUIZ", "PRACTICAL"], [
            [
                "title" => "What is Scrum?",
                "answers" => [
                    [
                        "name" => "A framework for Agile Development",
                        "correctAnswer" => true
                    ],
                    [
                        "name" => "A piece of software for Agile development",
                        "correctAnswer" => false
                    ],
                    [
                        "name" => "A way of coming to work",
                        "correctAnswer" => false
                    ],
                    [
                        "name" => "A new innovative way of writing code",
                        "correctAnswer" => false
                    ]
                ]
            ],
            [
                "title" => "What is the role of the Development Team?",
                "answers" => [
                    [
                        "name" => "To watch over the other scrum members",
                        "correctAnswer" => false
                    ],
                    [
                        "name" => "Just write code when they are told to",
                        "correctAnswer" => false
                    ],
                    [
                        "name" => "Work together and try to fix and code issues inside the current sprint",
                        "correctAnswer" => true
                    ],
                    [
                        "name" => "This term doesn't even exist?",
                        "correctAnswer" => false
                    ]
                ]
            ],
            [
                "title" => "What is the Product Backlog?",
                "answers" => [
                    [
                        "name" => "A store of files",
                        "correctAnswer" => false
                    ],
                    [
                        "name" => "A new way of monitoring products",
                        "correctAnswer" => false
                    ],
                    [
                        "name" => "I've never heard of it before?",
                        "correctAnswer" => true
                    ],
                    [
                        "name" => "A place to put issues when they are first encountered ready to be put on a sprint",
                        "correctAnswer" => true
                    ]
                ]
            ]
        ]);
    }

    private function createQuestion(int $lessonId, string $title, array $answers)
    {
        $id = DB::table('questions')->insertGetId([
            'lesson_id' => $lessonId,
            'title' => $title
        ]);


        foreach ($answers as $answer) {
            DB::table('question_answers')->insert([
                'question_id' => $id,
                'name' => $answer["name"],
                'correct_answer' => isset($answer["correctAnswer"]) ? $answer["correctAnswer"] : false
            ]);
        }
    }

    private function createLesson(array $values, array $includes, array $questions)
    {
        $id = DB::table('lessons')->insertGetId($values);

        foreach ($includes as $include) {
            switch ($include) {
                case "THEORY":
                    DB::table('include_lesson')->insert([
                        'lesson_id' => $id,
                        'include_id' => 1
                    ]);
                    break;
                case "PRACTICAL":
                    DB::table('include_lesson')->insert([
                        'lesson_id' => $id,
                        'include_id' => 2
                    ]);
                    break;
                case "QUIZ":
                    DB::table('include_lesson')->insert([
                        'lesson_id' => $id,
                        'include_id' => 3
                    ]);
                    break;
            }
        }

        foreach ($questions as $question) {
            $this->createQuestion($id, $question["title"], $question["answers"]);
        }
    }
}
